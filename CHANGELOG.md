[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | Changelog | Module Twitter API
================================================

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Changed
- Changed menu items @cmoeke

## [0.1.0] - 2020-09-29
### Added
- Added option to filter tweets by option "is_reply". @cmoeke
- Added return types to attributeLabels, rules and prefixableTableName @cmoeke

### Changed
- PHP dependency to 7.4 @cmoeke
- Changed ci config to handle new build branch @cmoeke

[Unreleased]: https://gitlab.com/finally-a-fast/fafcms-module-twitter-api/-/tree/master
[0.1.0]: https://gitlab.com/finally-a-fast/fafcms-module-twitter-api/-/tree/v0.1.0-alpha
