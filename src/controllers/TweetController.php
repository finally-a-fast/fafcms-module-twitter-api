<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-twitter-api/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-twitter-api
 * @see https://www.finally-a-fast.com/packages/fafcms-module-twitter-api/docs Documentation of fafcms-module-twitter-api
 * @since File available since Release 1.0.0
 */

namespace fafcms\twitterapi\controllers;

use fafcms\fafcms\models\QueueHelper;
use fafcms\helpers\DefaultController;
use fafcms\twitterapi\jobs\GetTweetsJob;
use fafcms\twitterapi\models\Resource;
use fafcms\twitterapi\models\Tweet;
use yii\web\Response;
use Yii;

/**
 * Class TweetController
 *
 * @package fafcms\twitterapi\controllers
 */
class TweetController extends DefaultController
{
    public static $modelClass = Tweet::class;

    /**
     * @return \yii\web\Response
     * @throws \yii\base\InvalidConfigException
     */
    public function actionFetch(): Response
    {
        foreach (Resource::find()->byProject('all')->byProjectLanguage('all')->all() as $resource) {
            QueueHelper::runJob(GetTweetsJob::class, [
                'resourceId' => $resource->id,
            ]);
        }

        Yii::$app->session->setFlash('success', Yii::t('fafcms-twitterapi', 'Added job for tweet download.'));

        return $this->goBack(Yii::$app->getRequest()->getReferrer());
    }
}
