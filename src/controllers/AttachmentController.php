<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-twitter-api/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-twitter-api
 * @see https://www.finally-a-fast.com/packages/fafcms-module-twitter-api/docs Documentation of fafcms-module-twitter-api
 * @since File available since Release 1.0.0
 */

namespace fafcms\twitterapi\controllers;

use fafcms\helpers\DefaultController;
use fafcms\twitterapi\models\Attachment;

/**
 * Class AttachmentController
 *
 * @package fafcms\twitterapi\controllers
 */
class AttachmentController extends DefaultController
{
    public static $modelClass = Attachment::class;
}
