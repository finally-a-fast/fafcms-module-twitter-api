<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-twitter-api/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-twitter-api
 * @see https://www.finally-a-fast.com/packages/fafcms-module-twitter-api/docs Documentation of fafcms-module-twitter-api
 * @since File available since Release 1.0.0
 */

namespace fafcms\twitterapi;

use fafcms\fafcms\inputs\Chips;
use fafcms\fafcms\inputs\NumberInput;
use fafcms\helpers\abstractions\PluginModule;
use fafcms\fafcms\inputs\Checkbox;
use fafcms\fafcms\inputs\TextInput;
use Yii;
use fafcms\helpers\classes\PluginSetting;
use fafcms\fafcms\components\FafcmsComponent;

/**
 * Class Module
 *
 * @package fafcms\twitterapi
 */
class Module extends PluginModule
{
}
