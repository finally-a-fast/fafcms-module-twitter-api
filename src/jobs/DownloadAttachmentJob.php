<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-twitter-api/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-twitter-api
 * @see https://www.finally-a-fast.com/packages/fafcms-module-twitter-api/docs Documentation of fafcms-module-twitter-api
 * @since File available since Release 1.0.0
 */

namespace fafcms\twitterapi\jobs;

use fafcms\filemanager\models\File;
use fafcms\filemanager\models\Filetype;
use fafcms\filemanager\Module;
use fafcms\twitterapi\models\Attachment;
use Yii;
use GuzzleHttp\Client;
use yii\helpers\FileHelper;

/**
 * Class DownloadAttachmentJob
 *
 * @package fafcms\twitterapi\jobs
 */
class DownloadAttachmentJob extends \yii\base\BaseObject implements \yii\queue\RetryableJobInterface
{
    public $attachmentId;
    public $attachmentPath;

    /**
     * @inheritdoc
     */
    public function execute($queue)
    {
        $attachment = Attachment::find()->where(['id' => $this->attachmentId])->one();

        if ($attachment === null) {
            return;
        }

        $url = $attachment->url;

        if ($url === null) {
            $url = $attachment->preview_image_url;
        }

        if ($url === null) {
            return;
        }

        $fileName = explode('/', $url);
        $fileName = end($fileName);

        $mimeType = FileHelper::getMimeTypeByExtension($fileName);
        $filegroup = Module::getLoadedModule()->getGetFilegroupByPath($this->attachmentPath ?? 'Twitter');

        $fileType = Filetype::find()->where(['mime_type' => $mimeType])->one();

        if ($fileType === null) {
            Yii::error('Unknown mime type. Attachment id: ' . $attachment->id . ' Mime type: '. $mimeType);
            return;
        }

        $file = new File([
            'filename' => substr($fileName, 0, strrpos($fileName, '.')),
            'filetype_id' => $fileType->id,
            'filegroup_id' => $filegroup->id,
            'size' => 0,
            'is_public' => 1,
        ]);

        FileHelper::createDirectory(File::getFileTarget($file));

        if (!$file->save()) {
            Yii::error('Cannot save file. Attachment id: ' . $attachment->id . ' Error: '. $file->getErrors());
            return;
        }

        $file = File::find()->where(['id' => $file->id])->one();

        $filePath = File::getFilePath($file);

        $client = new Client();
        $res = $client->get($url, [
            'sink' => $filePath,
        ]);

        if ($res->getStatusCode() === 200) {
            $file->size = filesize($filePath);

            if (!$file->save()) {
                Yii::error('Cannot update file size. Attachment id: ' . $attachment->id . ' Error: '. $file->getErrors());
                $file->delete();
                return;
            }

            $attachment->file_id = $file->id;

            if (!$attachment->save()) {
                Yii::error('Cannot update attachment. Attachment id: ' . $attachment->id . ' Error: '. $attachment->getErrors());
                $file->delete();
                return;
            }
        } else {
            $file->delete();
        }
    }

    /**
     * @inheritdoc
     */
    public function getTtr()
    {
        return 3 * (60 * 60);
    }

    /**
     * @inheritdoc
     */
    public function canRetry($attempt, $error)
    {
        return $attempt < 3;
    }
}
