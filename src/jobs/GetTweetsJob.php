<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-twitter-api/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-twitter-api
 * @see https://www.finally-a-fast.com/packages/fafcms-module-twitter-api/docs Documentation of fafcms-module-twitter-api
 * @since File available since Release 1.0.0
 */

namespace fafcms\twitterapi\jobs;

use fafcms\fafcms\models\QueueHelper;
use fafcms\twitterapi\models\Attachment;
use Yii;
use fafcms\twitterapi\models\Resource;
use fafcms\twitterapi\models\Tweet;
use GuzzleHttp\Client;
use yii\base\ErrorException;

/**
 * Class GetTweetsJob
 *
 * @package fafcms\twitterapi\jobs
 */
class GetTweetsJob extends \yii\base\BaseObject implements \yii\queue\RetryableJobInterface
{
    public $resourceId;

    /**
     * @inheritdoc
     */
    public function execute($queue)
    {
        $resource = Resource::find()->where(['id' => $this->resourceId])->one();

        if ($resource === null) {
            return;
        }

        $client = new Client([
            'base_uri' => 'https://api.twitter.com/2/',
            'headers' => ['Authorization' => 'Bearer ' . $resource->bearer]
        ]);

        $query = '';

        if (!empty($resource->from)) {
            $query .= 'from:' . $resource->from;
        }

        if ($resource->is_retweet !== null) {
            $query .= ' ' . ($resource->is_retweet === 2 ? '-' : '') . 'is:retweet';
        }

        if ($resource->is_reply !== null) {
            $query .= ' ' . ($resource->is_reply === 2 ? '-' : '') . 'is:reply';
        }

        if ($resource->has_mentions !== null) {
            $query .= ' ' . ($resource->has_mentions === 2 ? '-' : '') . 'has:mentions';
        }

        $res = $client->get('tweets/search/recent', [
            'query' => [
                'query' => $query,
                'tweet.fields' => 'attachments,author_id,in_reply_to_user_id,created_at',
                'max_results' => 100,
                'media.fields' => 'media_key,duration_ms,height,type,url,width,preview_image_url',
                'expansions' => 'attachments.media_keys',
            ],
        ]);

        $responseContent = $res->getBody()->getContents();
        $responseContent = json_decode($responseContent, true, 512, JSON_THROW_ON_ERROR);

        $attachmentTweetIds = [];

        foreach ($responseContent['data'] as $rawTweet) {
            $tweet = Tweet::find()->where(['twitter_id' => $rawTweet['id']])->one();

            if ($tweet === null) {
                $createdAt = new \DateTime($rawTweet['created_at']);

                $tweet = new Tweet([
                    'project_id' => $resource->project_id,
                    'projectlanguage_id' => $resource->projectlanguage_id,
                    'resource_id' => $this->resourceId,
                    'twitter_id' => $rawTweet['id'],
                    'text' => $rawTweet['text'],
                    'twitter_created_at' => $createdAt->format('Y-m-d H:i:s'),
                    'twitter_author_id' => $rawTweet['author_id'],
                ]);

                if (!$tweet->save()) {
                    Yii::error('Cannot save tweet. Twitter id: ' . $rawTweet['id'] . ' Error: '. $tweet->getErrors());
                }
            }

            foreach ($rawTweet['attachments']['media_keys'] ?? [] as $attachment) {
                $attachmentTweetIds[$attachment] = $tweet->id;
            }
        }

        if (isset($responseContent['includes'])) {
            foreach ($responseContent['includes']['media'] as $rawAttachment) {
                $attachment = Attachment::find()->where(['media_key' => $rawAttachment['media_key']])->one();

                if ($attachment === null) {
                    $attachment = new Attachment([
                        'project_id' => $resource->project_id,
                        'projectlanguage_id' => $resource->projectlanguage_id,
                        'tweet_id' => $attachmentTweetIds[$rawAttachment['media_key']] ?? null,
                        'media_key' => $rawAttachment['media_key'],
                        'type' => $rawAttachment['type'] ?? null,
                        'url' => $rawAttachment['url'] ?? null,
                        'preview_image_url' => $rawAttachment['preview_image_url'] ?? null,
                        'height' => $rawAttachment['height'] ?? null,
                        'width' => $rawAttachment['width'] ?? null,
                        'duration_ms' => $rawAttachment['duration_ms'] ?? null,
                    ]);

                    if (!$attachment->save()) {
                        Yii::error('Cannot save attachment. Media key: ' . $rawAttachment['media_key'] . ' Error: '. $attachment->getErrors());
                        continue;
                    }
                }

                if ($resource->download_attachments === 1 && $attachment->file_id === null) {
                    QueueHelper::runJob(DownloadAttachmentJob::class, [
                        'attachmentId' => $attachment->id,
                        'attachmentPath' => $resource->attachment_path,
                    ]);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function getTtr()
    {
        return 3 * (60 * 60);
    }

    /**
     * @inheritdoc
     */
    public function canRetry($attempt, $error)
    {
        return $attempt < 3;
    }
}
