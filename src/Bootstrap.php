<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-twitter-api/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-twitter-api
 * @see https://www.finally-a-fast.com/packages/fafcms-module-twitter-api/docs Documentation of fafcms-module-twitter-api
 * @since File available since Release 1.0.0
 */

namespace fafcms\twitterapi;

use fafcms\fafcms\components\FafcmsComponent;
use fafcms\twitterapi\models\Attachment;
use fafcms\twitterapi\models\Resource;
use fafcms\twitterapi\models\Tweet;
use Yii;
use yii\base\Application;
use fafcms\helpers\abstractions\PluginBootstrap;
use fafcms\helpers\abstractions\PluginModule;
use yii\base\BootstrapInterface;
use yii\i18n\PhpMessageSource;

/**
 * Class Bootstrap
 *
 * @package fafcms\twitterapi
 */
class Bootstrap extends PluginBootstrap implements BootstrapInterface
{
    public static $id = 'fafcms-twitterapi';
    public static $tablePrefix = 'fafcms-twitter_';

    protected function bootstrapTranslations(Application $app, PluginModule $module): bool
    {
        if (!isset($app->i18n->translations['fafcms-twitterapi'])) {
            $app->i18n->translations['fafcms-twitterapi'] = [
                'class' => PhpMessageSource::class,
                'basePath' => __DIR__ .'/messages',
                'forceTranslation' => true,
            ];
        }

        return true;
    }

    protected function bootstrapWebApp(Application $app, PluginModule $module): bool
    {
        Yii::$app->view->addNavigationItems(
            FafcmsComponent::NAVIGATION_SIDEBAR_LEFT,
            [
                'project' => [
                    'items' => [
                        'social'    => [
                            'after' => 'files',
                            'icon' => 'play-network-outline',
                            'label' => Yii::t('fafcms-core', 'Social media'),
                            'items' => [
                                'twitter' => [
                                    'icon' => 'twitter',
                                    'label' => Yii::t('fafcms-core', 'Twitter'),
                                    'items' => [
                                        'resource' => [
                                            'icon' => Resource::instance()->getEditData()['icon'],
                                            'label' => Resource::instance()->getEditData()['plural'],
                                            'url' => ['/' . Resource::instance()->getEditData()['url'] . '/index']
                                        ],
                                        'attachment' => [
                                            'icon' => Attachment::instance()->getEditData()['icon'],
                                            'label' => Attachment::instance()->getEditData()['plural'],
                                            'url' => ['/' . Attachment::instance()->getEditData()['url'] . '/index']
                                        ],
                                        'tweet' => [
                                            'icon' => Tweet::instance()->getEditData()['icon'],
                                            'label' => Tweet::instance()->getEditData()['plural'],
                                            'url' => ['/' . Tweet::instance()->getEditData()['url'] . '/index']
                                        ],
                                        'fetch' => [
                                            'icon' => 'download',
                                            'label' => 'Fetch data',
                                            'url' => ['/fafcms-twitterapi/tweet/fetch']
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        );

        $app->fafcms->addBackendUrlRules(self::$id, [
            '<controller:[a-zA-Z0-9\-]+>/<action:[a-zA-Z0-9\-]+>/<id:[a-zA-Z0-9\-\_]+>' => '<controller>/<action>',
            '<controller:[a-zA-Z0-9\-]+>/<action:[a-zA-Z0-9\-]+>' => '<controller>/<action>',
        ]);

        return true;
    }
}
