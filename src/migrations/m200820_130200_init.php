<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-twitter-api/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-twitter-api
 * @see https://www.finally-a-fast.com/packages/fafcms-module-twitter-api/docs Documentation of fafcms-module-twitter-api
 * @since File available since Release 1.0.0
 */

namespace fafcms\twitterapi\migrations;

use fafcms\twitterapi\models\Attachment;
use fafcms\twitterapi\models\Resource;
use fafcms\twitterapi\models\Tweet;
use yii\db\Migration;
use fafcms\filemanager\models\File;

/**
 * Class m200820_130200_init
 *
 * @package fafcms\sitemanager\migrations
 */
class m200820_130200_init extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(Resource::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('active'),
            'name' => $this->string(255)->notNull(),
            'remarks' => $this->text()->null()->defaultValue(null),
            'from' => $this->string(255)->null()->defaultValue(null),
            'is_retweet' => $this->tinyInteger(1)->unsigned()->null()->defaultValue(null),
            'has_mentions' => $this->tinyInteger(1)->unsigned()->null()->defaultValue(null),
            'download_attachments' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1),
            'attachment_path' => $this->string(255)->null()->defaultValue('Twitter Attachments'),
            'key' => $this->string(255)->null()->defaultValue(null),
            'secret' => $this->string(255)->null()->defaultValue(null),
            'bearer' => $this->string(255)->null()->defaultValue(null),
            'schedule' => $this->string(255)->notNull()->defaultValue('@daily'),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-resource-created_by', Resource::tableName(), ['created_by'], false);
        $this->createIndex('idx-resource-updated_by', Resource::tableName(), ['updated_by'], false);
        $this->createIndex('idx-resource-activated_by', Resource::tableName(), ['activated_by'], false);
        $this->createIndex('idx-resource-deactivated_by', Resource::tableName(), ['deactivated_by'], false);
        $this->createIndex('idx-resource-deleted_by', Resource::tableName(), ['deleted_by'], false);

        $this->createTable(Tweet::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'resource_id' =>  $this->integer(10)->unsigned()->notNull(),
            'twitter_id' => $this->string(255)->null()->defaultValue(null),
            'text' => $this->text()->null()->defaultValue(null),
            'twitter_created_at' => $this->datetime()->null()->defaultValue(null),
            'twitter_author_id' => $this->string(255)->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-tweet-resource_id', Tweet::tableName(), ['resource_id'], false);
        $this->createIndex('idx-tweet-created_by', Tweet::tableName(), ['created_by'], false);
        $this->createIndex('idx-tweet-updated_by', Tweet::tableName(), ['updated_by'], false);
        $this->createIndex('idx-tweet-activated_by', Tweet::tableName(), ['activated_by'], false);
        $this->createIndex('idx-tweet-deactivated_by', Tweet::tableName(), ['deactivated_by'], false);
        $this->createIndex('idx-tweet-deleted_by', Tweet::tableName(), ['deleted_by'], false);

        $this->createTable(Attachment::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'tweet_id' =>  $this->integer(10)->unsigned()->notNull(),
            'file_id' =>  $this->integer(10)->unsigned()->null()->defaultValue(null),
            'media_key' => $this->string(255)->null()->defaultValue(null),
            'type' => $this->string(255)->null()->defaultValue(null),
            'url' => $this->string(255)->null()->defaultValue(null),
            'preview_image_url' => $this->string(255)->null()->defaultValue(null),
            'height' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'width' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'duration_ms' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-attachment-tweet_id', Attachment::tableName(), ['tweet_id'], false);
        $this->createIndex('idx-attachment-file_id', Attachment::tableName(), ['file_id'], false);
        $this->createIndex('idx-attachment-created_by', Attachment::tableName(), ['created_by'], false);
        $this->createIndex('idx-attachment-updated_by', Attachment::tableName(), ['updated_by'], false);
        $this->createIndex('idx-attachment-activated_by', Attachment::tableName(), ['activated_by'], false);
        $this->createIndex('idx-attachment-deactivated_by', Attachment::tableName(), ['deactivated_by'], false);
        $this->createIndex('idx-attachment-deleted_by', Attachment::tableName(), ['deleted_by'], false);

        $this->addForeignKey('fk-tweet-resource_id', Tweet::tableName(), 'resource_id', Resource::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-attachment-tweet_id', Attachment::tableName(), 'tweet_id', Tweet::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-attachment-file_id', Attachment::tableName(), 'file_id', File::tableName(), 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-tweet-resource_id', Tweet::tableName());
        $this->dropForeignKey('fk-attachment-tweet_id', Attachment::tableName());
        $this->dropForeignKey('fk-attachment-file_id', Attachment::tableName());

        $this->dropTable(Resource::tableName());
        $this->dropTable(Tweet::tableName());
        $this->dropTable(Attachment::tableName());
    }
}
