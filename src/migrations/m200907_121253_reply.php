<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-twitter-api/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-twitter-api
 * @see https://www.finally-a-fast.com/packages/fafcms-module-twitter-api/docs Documentation of fafcms-module-twitter-api
 * @since File available since Release 1.0.0
 */

namespace fafcms\twitterapi\migrations;

use fafcms\twitterapi\models\Resource;
use yii\db\Migration;

/**
 * Class m200907_121253_reply
 *
 * @package fafcms\twitterapi\migrations
 */
class m200907_121253_reply extends Migration
{
    public function safeUp()
    {
        $this->addColumn(Resource::tableName(), 'is_reply', $this->tinyInteger(1)->unsigned()->null()->defaultValue(null)->after('is_retweet'));
    }

    public function safeDown()
    {
        $this->dropColumn(Resource::tableName(), 'is_reply');
    }
}
