<?php

namespace fafcms\twitterapi\models;

use fafcms\helpers\traits\MultilingualTrait;
use fafcms\twitterapi\abstracts\models\BaseTweet;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%tweet}}".
 *
 * @package fafcms\twitterapi\models
 */
class Tweet extends BaseTweet
{
    use MultilingualTrait;
}
