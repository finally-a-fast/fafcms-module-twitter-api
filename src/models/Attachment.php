<?php

namespace fafcms\twitterapi\models;

use fafcms\helpers\traits\MultilingualTrait;
use fafcms\twitterapi\abstracts\models\BaseAttachment;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%attachment}}".
 *
 * @package fafcms\twitterapi\models
 */
class Attachment extends BaseAttachment
{
    use MultilingualTrait;
}
