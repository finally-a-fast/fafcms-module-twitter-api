<?php

namespace fafcms\twitterapi\models;

use fafcms\helpers\traits\MultilingualTrait;
use fafcms\twitterapi\abstracts\models\BaseResource;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%resource}}".
 *
 * @package fafcms\twitterapi\models
 */
class Resource extends BaseResource
{
    use MultilingualTrait;
}
