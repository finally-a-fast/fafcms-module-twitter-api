<?php

namespace fafcms\twitterapi\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\ExtendedDropDownList,
    inputs\NumberInput,
    inputs\Textarea,
    inputs\TextInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
};
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use fafcms\twitterapi\{
    Bootstrap,
    models\Attachment,
    models\Resource,
};
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%tweet}}".
 *
 * @package fafcms\twitterapi\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property int $resource_id
 * @property string|null $twitter_id
 * @property string|null $text
 * @property string|null $twitter_created_at
 * @property string|null $twitter_author_id
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $activated_by
 * @property int|null $deactivated_by
 * @property int|null $deleted_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $activated_at
 * @property string|null $deactivated_at
 * @property string|null $deleted_at
 *
 * @property Resource $resource
 * @property Attachment[] $tweetAttachments
 */
abstract class BaseTweet extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/tweet';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'tweet';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-twitterapi', 'Tweets');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-twitterapi', 'Tweet');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['id'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.id'
            ])
            ->setSort([static::tableName() . '.id' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'resource_id' => static function($properties = []) {
                return Resource::getOptionProvider($properties)->getOptions();
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'resource_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('resource_id', false),
                'relationClassName' => Resource::class,
            ],
            'twitter_id' => [
                'type' => TextInput::class,
            ],
            'text' => [
                'type' => Textarea::class,
            ],
            'twitter_created_at' => [
                'type' => DateTimePicker::class,
            ],
            'twitter_author_id' => [
                'type' => TextInput::class,
            ],
            'created_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                        'link' => true,
                    ],
                ],
                'resource_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'resource_id',
                        'sort' => 2,
                        'link' => true,
                    ],
                ],
                'twitter_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'twitter_id',
                        'sort' => 3,
                    ],
                ],
                'text' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'text',
                        'sort' => 4,
                    ],
                ],
                'twitter_created_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'twitter_created_at',
                        'sort' => 5,
                    ],
                ],
                'twitter_author_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'twitter_author_id',
                        'sort' => 6,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-resource_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'resource_id',
                                                    ],
                                                ],
                                                'field-twitter_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'twitter_id',
                                                    ],
                                                ],
                                                'field-text' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'text',
                                                    ],
                                                ],
                                                'field-twitter_created_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'twitter_created_at',
                                                    ],
                                                ],
                                                'field-twitter_author_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'twitter_author_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%tweet}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'required-resource_id' => ['resource_id', 'required'],
            'integer-resource_id' => ['resource_id', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'integer-activated_by' => ['activated_by', 'integer'],
            'integer-deactivated_by' => ['deactivated_by', 'integer'],
            'integer-deleted_by' => ['deleted_by', 'integer'],
            'string-text' => ['text', 'string'],
            'date-twitter_created_at' => ['twitter_created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'twitter_created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-activated_at' => ['activated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'activated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deactivated_at' => ['deactivated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deactivated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deleted_at' => ['deleted_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deleted_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'string-twitter_id' => ['twitter_id', 'string', 'max' => 255],
            'string-twitter_author_id' => ['twitter_author_id', 'string', 'max' => 255],
            'exist-resource_id' => [['resource_id'], 'exist', 'skipOnError' => true, 'targetClass' => Resource::class, 'targetAttribute' => ['resource_id' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-twitterapi', 'ID'),
            'resource_id' => Yii::t('fafcms-twitterapi', 'Resource ID'),
            'twitter_id' => Yii::t('fafcms-twitterapi', 'Twitter ID'),
            'text' => Yii::t('fafcms-twitterapi', 'Text'),
            'twitter_created_at' => Yii::t('fafcms-twitterapi', 'Twitter Created At'),
            'twitter_author_id' => Yii::t('fafcms-twitterapi', 'Twitter Author ID'),
            'created_by' => Yii::t('fafcms-twitterapi', 'Created By'),
            'updated_by' => Yii::t('fafcms-twitterapi', 'Updated By'),
            'activated_by' => Yii::t('fafcms-twitterapi', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-twitterapi', 'Deactivated By'),
            'deleted_by' => Yii::t('fafcms-twitterapi', 'Deleted By'),
            'created_at' => Yii::t('fafcms-twitterapi', 'Created At'),
            'updated_at' => Yii::t('fafcms-twitterapi', 'Updated At'),
            'activated_at' => Yii::t('fafcms-twitterapi', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-twitterapi', 'Deactivated At'),
            'deleted_at' => Yii::t('fafcms-twitterapi', 'Deleted At'),
        ]);
    }

    /**
     * Gets query for [[Resource]].
     *
     * @return ActiveQuery
     */
    public function getResource(): ActiveQuery
    {
        return $this->hasOne(Resource::class, [
            'id' => 'resource_id',
        ]);
    }

    /**
     * Gets query for [[TweetAttachments]].
     *
     * @return ActiveQuery
     */
    public function getTweetAttachments(): ActiveQuery
    {
        return $this->hasMany(Attachment::class, [
            'tweet_id' => 'id',
        ]);
    }
}
