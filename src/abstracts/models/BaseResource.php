<?php

namespace fafcms\twitterapi\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\DropDownList,
    inputs\NumberInput,
    inputs\SwitchCheckbox,
    inputs\Textarea,
    inputs\TextInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
};
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use fafcms\twitterapi\{
    Bootstrap,
    models\Tweet,
};
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%resource}}".
 *
 * @package fafcms\twitterapi\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property string $status
 * @property string $name
 * @property string|null $remarks
 * @property string|null $from
 * @property int|null $is_retweet
 * @property int|null $is_reply
 * @property int|null $has_mentions
 * @property int $download_attachments
 * @property string|null $attachment_path
 * @property string|null $key
 * @property string|null $secret
 * @property string|null $bearer
 * @property string $schedule
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $activated_by
 * @property int|null $deactivated_by
 * @property int|null $deleted_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $activated_at
 * @property string|null $deactivated_at
 * @property string|null $deleted_at
 *
 * @property Tweet[] $resourceTweets
 */
abstract class BaseResource extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/resource';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'resource';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-twitterapi', 'Resources');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-twitterapi', 'Resource');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['name'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.name'
            ])
            ->setSort([static::tableName() . '.name' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'status' => [
                static::STATUS_ACTIVE => Yii::t('fafcms-core', 'Active'),
                static::STATUS_INACTIVE => Yii::t('fafcms-core', 'Inactive'),
            ],
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'status' => [
                'type' => DropDownList::class,
                'items' => $this->getAttributeOptions('status', false),
            ],
            'name' => [
                'type' => TextInput::class,
            ],
            'remarks' => [
                'type' => Textarea::class,
            ],
            'from' => [
                'type' => TextInput::class,
            ],
            'is_retweet' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'is_reply' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'has_mentions' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'download_attachments' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'attachment_path' => [
                'type' => TextInput::class,
            ],
            'key' => [
                'type' => TextInput::class,
            ],
            'secret' => [
                'type' => TextInput::class,
            ],
            'bearer' => [
                'type' => TextInput::class,
            ],
            'schedule' => [
                'type' => TextInput::class,
            ],
            'created_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                    ],
                ],
                'status' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'status',
                        'sort' => 2,
                    ],
                ],
                'name' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'name',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                'remarks' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'remarks',
                        'sort' => 4,
                    ],
                ],
                'from' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'from',
                        'sort' => 5,
                    ],
                ],
                'is_retweet' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'is_retweet',
                        'sort' => 6,
                    ],
                ],
                'is_reply' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'is_reply',
                        'sort' => 7,
                    ],
                ],
                'has_mentions' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'has_mentions',
                        'sort' => 8,
                    ],
                ],
                'download_attachments' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'download_attachments',
                        'sort' => 9,
                    ],
                ],
                'attachment_path' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'attachment_path',
                        'sort' => 10,
                    ],
                ],
                'key' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'key',
                        'sort' => 11,
                    ],
                ],
                'secret' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'secret',
                        'sort' => 12,
                    ],
                ],
                'bearer' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'bearer',
                        'sort' => 13,
                    ],
                ],
                'schedule' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'schedule',
                        'sort' => 14,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                                'field-remarks' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'remarks',
                                                    ],
                                                ],
                                                'field-from' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'from',
                                                    ],
                                                ],
                                                'field-is_retweet' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'is_retweet',
                                                    ],
                                                ],
                                                'field-is_reply' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'is_reply',
                                                    ],
                                                ],
                                                'field-has_mentions' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'has_mentions',
                                                    ],
                                                ],
                                                'field-download_attachments' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'download_attachments',
                                                    ],
                                                ],
                                                'field-attachment_path' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'attachment_path',
                                                    ],
                                                ],
                                                'field-key' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'key',
                                                    ],
                                                ],
                                                'field-secret' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'secret',
                                                    ],
                                                ],
                                                'field-bearer' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'bearer',
                                                    ],
                                                ],
                                                'field-schedule' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'schedule',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%resource}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'required-name' => ['name', 'required'],
            'string-remarks' => ['remarks', 'string'],
            'boolean-is_retweet' => ['is_retweet', 'boolean'],
            'boolean-is_reply' => ['is_reply', 'boolean'],
            'boolean-has_mentions' => ['has_mentions', 'boolean'],
            'boolean-download_attachments' => ['download_attachments', 'boolean'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'integer-activated_by' => ['activated_by', 'integer'],
            'integer-deactivated_by' => ['deactivated_by', 'integer'],
            'integer-deleted_by' => ['deleted_by', 'integer'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-activated_at' => ['activated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'activated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deactivated_at' => ['deactivated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deactivated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deleted_at' => ['deleted_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deleted_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'string-status' => ['status', 'string', 'max' => 255],
            'string-name' => ['name', 'string', 'max' => 255],
            'string-from' => ['from', 'string', 'max' => 255],
            'string-attachment_path' => ['attachment_path', 'string', 'max' => 255],
            'string-key' => ['key', 'string', 'max' => 255],
            'string-secret' => ['secret', 'string', 'max' => 255],
            'string-bearer' => ['bearer', 'string', 'max' => 255],
            'string-schedule' => ['schedule', 'string', 'max' => 255],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-twitterapi', 'ID'),
            'status' => Yii::t('fafcms-twitterapi', 'Status'),
            'name' => Yii::t('fafcms-twitterapi', 'Name'),
            'remarks' => Yii::t('fafcms-twitterapi', 'Remarks'),
            'from' => Yii::t('fafcms-twitterapi', 'From'),
            'is_retweet' => Yii::t('fafcms-twitterapi', 'Is Retweet'),
            'is_reply' => Yii::t('fafcms-twitterapi', 'Is Reply'),
            'has_mentions' => Yii::t('fafcms-twitterapi', 'Has Mentions'),
            'download_attachments' => Yii::t('fafcms-twitterapi', 'Download Attachments'),
            'attachment_path' => Yii::t('fafcms-twitterapi', 'Attachment Path'),
            'key' => Yii::t('fafcms-twitterapi', 'Key'),
            'secret' => Yii::t('fafcms-twitterapi', 'Secret'),
            'bearer' => Yii::t('fafcms-twitterapi', 'Bearer'),
            'schedule' => Yii::t('fafcms-twitterapi', 'Schedule'),
            'created_by' => Yii::t('fafcms-twitterapi', 'Created By'),
            'updated_by' => Yii::t('fafcms-twitterapi', 'Updated By'),
            'activated_by' => Yii::t('fafcms-twitterapi', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-twitterapi', 'Deactivated By'),
            'deleted_by' => Yii::t('fafcms-twitterapi', 'Deleted By'),
            'created_at' => Yii::t('fafcms-twitterapi', 'Created At'),
            'updated_at' => Yii::t('fafcms-twitterapi', 'Updated At'),
            'activated_at' => Yii::t('fafcms-twitterapi', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-twitterapi', 'Deactivated At'),
            'deleted_at' => Yii::t('fafcms-twitterapi', 'Deleted At'),
        ]);
    }

    /**
     * Gets query for [[ResourceTweets]].
     *
     * @return ActiveQuery
     */
    public function getResourceTweets(): ActiveQuery
    {
        return $this->hasMany(Tweet::class, [
            'resource_id' => 'id',
        ]);
    }
}
