<?php

namespace fafcms\twitterapi\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\ExtendedDropDownList,
    inputs\NumberInput,
    inputs\TextInput,
    inputs\UrlInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
};
use fafcms\filemanager\{
    inputs\FileSelect,
    models\File,
};
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use fafcms\twitterapi\{
    Bootstrap,
    models\Tweet,
};
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%attachment}}".
 *
 * @package fafcms\twitterapi\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property int $tweet_id
 * @property int|null $file_id
 * @property string|null $media_key
 * @property string|null $type
 * @property string|null $url
 * @property string|null $preview_image_url
 * @property int|null $height
 * @property int|null $width
 * @property int|null $duration_ms
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $activated_by
 * @property int|null $deactivated_by
 * @property int|null $deleted_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $activated_at
 * @property string|null $deactivated_at
 * @property string|null $deleted_at
 *
 * @property File $file
 * @property Tweet $tweet
 */
abstract class BaseAttachment extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/attachment';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'attachment';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-twitterapi', 'Attachments');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-twitterapi', 'Attachment');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['id'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.id'
            ])
            ->setSort([static::tableName() . '.id' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'tweet_id' => static function($properties = []) {
                return Tweet::getOptionProvider($properties)->getOptions();
            },
            'file_id' => static function(...$params) {
                return File::getOptions(...$params);
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'tweet_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('tweet_id', false),
                'relationClassName' => Tweet::class,
            ],
            'file_id' => [
                'type' => FileSelect::class,
                'external' => false,
            ],
            'media_key' => [
                'type' => FileSelect::class,
            ],
            'type' => [
                'type' => TextInput::class,
            ],
            'url' => [
                'type' => UrlInput::class,
            ],
            'preview_image_url' => [
                'type' => UrlInput::class,
            ],
            'height' => [
                'type' => NumberInput::class,
            ],
            'width' => [
                'type' => NumberInput::class,
            ],
            'duration_ms' => [
                'type' => NumberInput::class,
            ],
            'created_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                        'link' => true,
                    ],
                ],
                'tweet_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'tweet_id',
                        'sort' => 2,
                        'link' => true,
                    ],
                ],
                'file_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'file_id',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                'media_key' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'media_key',
                        'sort' => 4,
                        'link' => true,
                    ],
                ],
                'type' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'type',
                        'sort' => 5,
                    ],
                ],
                'url' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'url',
                        'sort' => 6,
                    ],
                ],
                'preview_image_url' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'preview_image_url',
                        'sort' => 7,
                    ],
                ],
                'height' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'height',
                        'sort' => 8,
                    ],
                ],
                'width' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'width',
                        'sort' => 9,
                    ],
                ],
                'duration_ms' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'duration_ms',
                        'sort' => 10,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-tweet_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'tweet_id',
                                                    ],
                                                ],
                                                'field-file_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'file_id',
                                                    ],
                                                ],
                                                'field-media_key' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'media_key',
                                                    ],
                                                ],
                                                'field-type' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'type',
                                                    ],
                                                ],
                                                'field-url' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'url',
                                                    ],
                                                ],
                                                'field-preview_image_url' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'preview_image_url',
                                                    ],
                                                ],
                                                'field-height' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'height',
                                                    ],
                                                ],
                                                'field-width' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'width',
                                                    ],
                                                ],
                                                'field-duration_ms' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'duration_ms',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%attachment}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'required-tweet_id' => ['tweet_id', 'required'],
            'integer-tweet_id' => ['tweet_id', 'integer'],
            'integer-file_id' => ['file_id', 'integer'],
            'integer-height' => ['height', 'integer'],
            'integer-width' => ['width', 'integer'],
            'integer-duration_ms' => ['duration_ms', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'integer-activated_by' => ['activated_by', 'integer'],
            'integer-deactivated_by' => ['deactivated_by', 'integer'],
            'integer-deleted_by' => ['deleted_by', 'integer'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-activated_at' => ['activated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'activated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deactivated_at' => ['deactivated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deactivated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deleted_at' => ['deleted_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deleted_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'string-media_key' => ['media_key', 'string', 'max' => 255],
            'string-type' => ['type', 'string', 'max' => 255],
            'string-url' => ['url', 'string', 'max' => 255],
            'string-preview_image_url' => ['preview_image_url', 'string', 'max' => 255],
            'exist-file_id' => [['file_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::class, 'targetAttribute' => ['file_id' => 'id']],
            'exist-tweet_id' => [['tweet_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tweet::class, 'targetAttribute' => ['tweet_id' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-twitterapi', 'ID'),
            'tweet_id' => Yii::t('fafcms-twitterapi', 'Tweet ID'),
            'file_id' => Yii::t('fafcms-twitterapi', 'File ID'),
            'media_key' => Yii::t('fafcms-twitterapi', 'Media Key'),
            'type' => Yii::t('fafcms-twitterapi', 'Type'),
            'url' => Yii::t('fafcms-twitterapi', 'Url'),
            'preview_image_url' => Yii::t('fafcms-twitterapi', 'Preview Image Url'),
            'height' => Yii::t('fafcms-twitterapi', 'Height'),
            'width' => Yii::t('fafcms-twitterapi', 'Width'),
            'duration_ms' => Yii::t('fafcms-twitterapi', 'Duration Ms'),
            'created_by' => Yii::t('fafcms-twitterapi', 'Created By'),
            'updated_by' => Yii::t('fafcms-twitterapi', 'Updated By'),
            'activated_by' => Yii::t('fafcms-twitterapi', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-twitterapi', 'Deactivated By'),
            'deleted_by' => Yii::t('fafcms-twitterapi', 'Deleted By'),
            'created_at' => Yii::t('fafcms-twitterapi', 'Created At'),
            'updated_at' => Yii::t('fafcms-twitterapi', 'Updated At'),
            'activated_at' => Yii::t('fafcms-twitterapi', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-twitterapi', 'Deactivated At'),
            'deleted_at' => Yii::t('fafcms-twitterapi', 'Deleted At'),
        ]);
    }

    /**
     * Gets query for [[File]].
     *
     * @return ActiveQuery
     */
    public function getFile(): ActiveQuery
    {
        return $this->hasOne(File::class, [
            'id' => 'file_id',
        ]);
    }

    /**
     * Gets query for [[Tweet]].
     *
     * @return ActiveQuery
     */
    public function getTweet(): ActiveQuery
    {
        return $this->hasOne(Tweet::class, [
            'id' => 'tweet_id',
        ]);
    }
}
